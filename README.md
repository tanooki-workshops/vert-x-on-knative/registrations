# vert-x-on-knative registrations

- **Topic**: Deploy Vert-x Microservice on Knative with GitLab CI
- **Date**: Tuesday, March 8th - 07:00 PM

## How to register to the workshop?

It's simple: you just need to create an issue in this project.

> the 10 first created issues will be kept

